package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
