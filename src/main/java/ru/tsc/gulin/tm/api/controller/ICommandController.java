package ru.tsc.gulin.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showWelcome();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
