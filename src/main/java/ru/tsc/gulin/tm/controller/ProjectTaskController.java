package ru.tsc.gulin.tm.controller;

import ru.tsc.gulin.tm.api.controller.IProjectTaskController;
import ru.tsc.gulin.tm.api.service.IProjectTaskService;
import ru.tsc.gulin.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void addTaskToProjectById() {
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void removeTaskFromProjectById() {
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.removeProjectById(projectId);
    }

}
